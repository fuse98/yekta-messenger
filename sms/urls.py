from django.urls import path
from . import views

urlpatterns = [
    path('singelsms/', views.SingleSMSView.as_view()),
    path('multiplesms/', views.MultipleSMSView.as_view()),
]
