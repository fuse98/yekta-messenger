from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from kavenegar import *

KAVENEGAR_APIKEY = "485375444E643041306541594752355A4B4131486D6E396B7342534D52786833442B59584935466C4471553D"

def send_sms(numbers, message):
  print(numbers)
  if(len(numbers) == 0):
    return "no valid customers provided"
  receptors = ''
  for n in numbers:
    receptors += n
    receptors += ','
  try:
    api = KavenegarAPI(KAVENEGAR_APIKEY)
    params = {
      'receptor': receptors,
      'message': message
    }
    response = api.sms_send(params) 
  except APIException as e:
    print(e)
    return "failed to send to kavenegar"
  except HTTPException as e:
    print(e)
    return "failed to send to kavenegar"

  return response

class SingleSMSView(APIView):

  def get(self, request):
    try:
      uid = request.data['uid']
      reciver_numbers = self.get_numbers(request.user.business, uid)
      response = send_sms(reciver_numbers, request.data['message'])
      return Response(response, status = status.HTTP_200_OK)
    except:
      return Response("bad request", status = status.HTTP_400_BAD_REQUEST)

  @staticmethod
  def get_numbers(business, uid):
    numbers = []
    if(uid[:1] == "S"):
      try:
        number = business.customers.get(uid = uid).phone_number
        numbers.append(number)
      except:
       pass
    elif(uid[:1] == "G"):
      try:
        customers = business.groups.get(uid = uid).customers.all()
        for c in customers:
          numbers.append(c.phone_number)
      except:
        pass
    return numbers




class MultipleSMSView(APIView):

  def get(self, request):    
    try:
      uids = request.data['uids']
      reciver_numbers = self.get_numbers(request.user.business, uids)
      response = send_sms(reciver_numbers, request.data['message'])
      return Response(response, status = status.HTTP_200_OK)
    except:
      return Response("bad request", status = status.HTTP_400_BAD_REQUEST)
      
  @staticmethod
  def get_numbers(business, uids):
    numbers = {}
    for uid in uids:    
      try:
        customers = business.groups.get(uid = uid).customers.all()
        for c in customers:
          if(not (c.phone_number in numbers)):
            numbers[c.phone_number] = 1
      except:
        continue
    return list(numbers.keys())
