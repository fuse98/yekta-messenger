from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Business(models.Model):
  user = models.OneToOneField(User, on_delete = models.CASCADE, related_name = 'business')
  name = models.CharField(max_length = 50)
  stablish_year = models.DateField()
  address = models.TextField()
  phone_number = models.CharField(max_length = 11)
