from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .serializers import UserSerializer, BusinessSerializer
from .models import Business, User

class BusinessView(APIView):

  def get_permissions(self):
    if(self.request.method == 'POST'):
      return [AllowAny()]
    elif(self.request.method == 'PUT'):
      return [IsAuthenticated()]
    else:
      return [AllowAny()]

  def post(self, request):
    serializer = BusinessSerializer(data = request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status = status.HTTP_201_CREATED)

    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
  
  def put(self, request):
    try:
      business = request.user.business
      serializer = BusinessSerializer(business, data = request.data)
      if(serializer.is_valid()):
        serializer.save()
        return Response(serializer.data)
      return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
    except:
      return Response("invalid request", status = status.HTTP_400_BAD_REQUEST)
      