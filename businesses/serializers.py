import hashlib
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .models import Business, User

class UserSerializer(serializers.ModelSerializer):
  token =  serializers.SerializerMethodField('find_token')
  class Meta:
    model = User
    fields = ['username', 'email', 'password', 'token']
  def find_token(self, obj):
    token = Token.objects.get(user = obj.id)
    return token.key

class BusinessSerializer(serializers.ModelSerializer):
  user = UserSerializer(required = True)
  class Meta:
    model = Business
    fields = ['id', 'user', 'name', 'phone_number', 'stablish_year', 'address']

  def create(self, validated_data):
    user_data = validated_data.pop('user')
    user = UserSerializer.create(UserSerializer(), validated_data=user_data)
    Token.objects.create(user = user)
    business = Business.objects.create(user = user, name = validated_data.pop('name'),
      phone_number = validated_data.pop('phone_number'), stablish_year = validated_data.pop('stablish_year'),
      address = validated_data.pop('address'))
    return business

  def update(self, instance, validated_data):
    user_data = validated_data.pop('user')
    instance.user = UserSerializer.update(UserSerializer(), instance.user, validated_data=user_data)
    instance.name = validated_data.get('name', instance.name)
    instance.phone_number = validated_data.get('phone_number', instance.name)
    instance.stablish_year = validated_data.get('stablish_year', instance.name)
    instance.address = validated_data.get('address', instance.name)
    return instance
