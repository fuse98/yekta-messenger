from django.urls import path
from . import views

urlpatterns = [
    path('businesses/', views.BusinessView.as_view()),
]