from django.db import models
from businesses.models import Business

class CustomerGroup(models.Model):
  uid = models.TextField(blank = True, unique = True)
  business = models.ForeignKey(Business, on_delete = models.CASCADE, related_name = 'groups')
  name = models.CharField(max_length=150)

class Customer(models.Model):  
  uid = models.TextField(blank = True, unique = True)
  business = models.ForeignKey(Business, on_delete = models.CASCADE, related_name = 'customers')
  name = models.CharField(max_length = 150)
  phone_number = models.CharField(max_length = 11)
  group = models.ManyToManyField(CustomerGroup, related_name = 'customers')
