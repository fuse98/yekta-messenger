from django.urls import path
from . import views

urlpatterns = [
    path('customers/', views.CustomerView.as_view()),
    path('groups/', views.CustomerGroupView.as_view()),
]