from django.contrib import admin
from .models import Customer, CustomerGroup

# Register your models here.

admin.site.register(Customer)
admin.site.register(CustomerGroup)
