import hashlib
from rest_framework import serializers


from .models import Customer, CustomerGroup


class CustomerSerializer(serializers.ModelSerializer):
  class Meta:
    model = Customer
    fields = ['uid', 'name', 'phone_number',]
  def create(self, validated_data):
    business = self.context['business']
    customer = Customer.objects.create(uid = self.create_uid(business.id, validated_data['phone_number']),
      business = business, name = validated_data.pop('name'), phone_number = validated_data.pop('phone_number'))
    return customer
  
  def create_uid(self, business_id, phone_number):
    uid = phone_number + str(business_id)
    uid = hashlib.md5(uid.encode())
    uid = "S_" + uid.hexdigest()
    return uid
    

class CustomerGroupSerializer(serializers.ModelSerializer):
  class Meta:
    model = CustomerGroup
    fields = ['uid', 'name']
  def create(self, validated_data):
    business = self.context['business']
    customer_uids = self.context['customer_uids']
    customers = business.customers.filter(uid__in = customer_uids)
    group = CustomerGroup.objects.create(uid = self.create_uid(business.id, validated_data['name']),
      name = validated_data.pop('name'), business = business)
    for c in customers:
      group.customers.add(c)
    return group
  
  def create_uid(self, business_id, name):
    uid = name + str(business_id)
    uid = hashlib.md5(uid.encode())
    uid = "G_" + uid.hexdigest()
    return uid
    