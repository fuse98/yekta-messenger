from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .serializers import CustomerSerializer, CustomerGroupSerializer

class CustomerView(APIView):

  def get(self, request):
    business = request.user.business
    customers = business.customers
    serializer = CustomerSerializer(customers, many = True)
    return Response(serializer.data)

  def post(self, request):  
    try:
      business = request.user.business
      if(self.customer_is_new(business, request.data['phone_number'])):
        serializer = CustomerSerializer(data = request.data, context = {'business': business})
        if(serializer.is_valid()):
          serializer.save()
          return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.error_messages, status = status.HTTP_400_BAD_REQUEST)
      return Response("duplicated record", status = status.HTTP_400_BAD_REQUEST)
    except:
      return Response("invalid request", status = status.HTTP_400_BAD_REQUEST)
    

  @staticmethod
  def customer_is_new(business, phone_number):
    try:
      customer = business.customers.get(phone_number = phone_number)
    except:
      customer = False
    return not customer

class CustomerGroupView(APIView):
  
  def get(self, request):    
    business = request.user.business
    groups = business.groups
    serializer = CustomerGroupSerializer(groups, many = True)
    return Response(serializer.data)

  def post(self, request):  
    try:
      business = request.user.business
      serializer = CustomerGroupSerializer(data = request.data,
        context = {'business': business, 'customer_uids': request.data['customers']})
      if(self.group_is_new(business, request.data['name'])):
        if(serializer.is_valid()):
          serializer.save()
          return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response("duplicated record", status = status.HTTP_400_BAD_REQUEST)
    except:
      pass
    return Response(serializer.error_messages, status = status.HTTP_400_BAD_REQUEST)

  @staticmethod
  def group_is_new(business, name):
    try:
      group = business.groups.get(name = name)
    except:
      group = False
    return not group
